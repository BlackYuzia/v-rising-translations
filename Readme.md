# **V Rising Translations**

> **Переклад більше не актуальний.** aka deprecated 
> 
> З **08.07.2022** з'явився офіційний переклад на укр. мову. [**Детальніше**](https://translate.playvrising.com/project/v-rising-game/uk). Він завантажується й буде оновлюватись автоматично з оновленням(и) гри.
>
> Якщо знайшли помилку в оф. перекладі / локалізації гри, потрібно заходити на сайт та пропунувати виправлення.

## ~~**English**~~
### ~~**Install**~~

~~Translation file should be stored in to `X:\...\V Rising\VRising_Data\StreamingAssets\Localization\{language}.json`~~

### ~~**Translations List**~~

- ~~**Ukraine** `(By Google, Black_Yuzia)`~~
- ~~**Russian** `(By JloKo. Used only as context base for Ukraine)`~~
- ~~**English** `(Original. Used only as base for any other)`~~

### ~~**FAQ**~~

- ~~**Found a mistake?** Make a issue / PR.~~
- ~~**Have suggestion(s)?** Make a issue / PR.~~
- ~~**Anything else?** Make a issue.~~

~~## **Українською**~~

### ~~**Інсталяція**~~

~~Потрібний файл необхідно помістити в `X:\...\V Rising\VRising_Data\StreamingAssets\Localization\{language}.json`~~

### ~~**Список перекладів**~~

- ~~**Український** `(By Google, Black_Yuzia)`~~
- ~~**Російський** `(By JloKo. Used only as context base for Ukraine)`~~
- ~~**Англійський** `(Original. Used only as base for any other)`~~

### ~~**ЧАВо**~~

- ~~**Знайшли помилку?** Створіть issue / PR.~~
- ~~**Маєте пропозицію?** Створіть issue / PR.~~
- ~~**Щось інше?** Створіть issue.~~