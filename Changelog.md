# Changelog 

There you can find latest changes (or not)

### **[0.0.4]** `[10.07.2022]`

- upload offical ua translations (08.07.2022)
- upload changes in offical en translations (08.07.2022)
- upload changes in offical ru translations (08.07.2022)

> Suppose, this repo now could be be **deprecated** by a reason what we have now offical translation.


### **[0.0.3]** `[02.07.2022]`

- small changes/fixes in ua translations
- upload changes in offical en translations
- upload changes in offical ru translations

### **[0.0.2]** `[17.06.2022]`

16.06.2022 - date when translations has been updated by V Rising developers  / дата когда были обновлены переводы от разработчиков V Rising

- **EN**
  - Add offical ru translate (16.06.2022)
  - Add a few ru translates (other authors)
  - Updated en translate (16.06.2022)
  - Updated ua translate (16.06.2022)
- **RU**
  - Добавлен оф. русский перевод (16.06.2022)
  - Добавлен рус. перевод от других авторов
    - Не уверен что актуальная версия, рекомендую использовать или оф. рус. или укр. перевод
  - Обновлен англ. перевод (16.06.2022)
  - Обновлен укр. перевод (16.06.2022)

### **[0.0.1]** `[30.05.2022]`

- Добавлено переклад української мови (Google).
- Невеликі зміни UA перекладу під контекст.